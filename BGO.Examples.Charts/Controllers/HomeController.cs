﻿using System.Web.Mvc;

namespace BGO.Examples.Charts.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}