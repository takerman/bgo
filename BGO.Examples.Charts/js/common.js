﻿$(function () {
    var ddlCategories = $("#ddlCategories").kendoDropDownList({
        dataValueField: "CategoryID",
        dataTextField: "CategoryName",
        dataSource: data.categories,
        dataBound: function () {
            data.categories.products = data.products.filter({ field: "CategoryID", operator: "eq", value: this.value().toString() });
        },
        change: function (e) {
            data.categories.products = data.products.filter({ field: "CategoryID", operator: "eq", value: this.value().toString() });
        }
    });
    var pieCategories = $("#pieCategories").kendoChart({
        title: {
            position: "bottom",
            text: "Category Products"
        },
        legend: {
            visible: false
        },
        chartArea: {
            background: ""
        },
        dataSource: data.categories,
        seriesDefaults: {
            labels: {
                visible: true,
                background: "transparent",
                template: "#= dataItem.CategoryName #"
            }
        },
        series: [{
            type: "pie",
            field: "CategoryID",
            categoryField: "CategoryID"
        }],
        seriesClick: function (e) {
            ddlCategories = $("#ddlCategories").data("kendoDropDownList");
            ddlCategories.select(e.value - 1);
            ddlCategories.trigger("change");
        },
        tooltip: {
            visible: true,
            template: "${dataItem.CategoryName} - ${dataItem.CategoryID}"
        }
    });

    var gridProducts = $("#gridProducts").kendoGrid({
        autoSync: true,
        dataSource: data.categories.products,
        height: 300,
        filterable: true,
        sortable: true,
        pageable: true,
        dataBound: function (e) {
            $('tr').each(function (e, o, q) {
                if ($(this).find("td:last").text() == "true") {
                    $(this).addClass('discontinued');
                }
            })
        },
        columns: [
            {
                field: "ProductName.toUpperCase()",
                title: "Name",
                format: "{0}"
            },
            {
                field: "UnitPrice",
                title: "Price",
                format: "{0} €"
            },
            {
                field: "UnitsInStock",
                title: "In Stock"
            },
            {
                field: "QuantityPerUnit",
                title: "Quantity"
            },
            {
                field: "Discontinued",
                title: "Discontinued",
                hidden: true
            }
        ]
    });
    $("#btnDownloadSource").click(function () {
        window.open("http://cdn.takerman.net/files/BGO.zip", "_blank");
    });
});
