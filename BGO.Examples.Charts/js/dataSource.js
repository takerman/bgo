﻿var data = {
    products: new kendo.data.DataSource({
        autoSync: true,
        type: "odata",
        transport: {
            read: {
                url: "http://demos.telerik.com/kendo-ui/service/Northwind.svc/Products",
                dataType: "jsonp"
            }
        },
        schema: {
            model: {
                fields: {
                    ProductID: { type: "number" },
                    ProductName: { type: "string" },
                    SupplierID: { type: "number" },
                    CategoryID: { type: "number" },
                    QuantityPerUnit: { type: "string" },
                    UnitPrice: { type: "decimal" },
                    UnitsInStock: { type: "number" },
                    UnitsOnOrder: { type: "number" },
                    ReorderLevel: { type: "number" },
                    Discontinued: { type: "bool" }
                }
            }
        },
        pageSize: 20,
        serverPaging: true,
        serverSorting: true,
        serverFiltering: false
    }),
    categories: new kendo.data.DataSource({
        autoSync: true,
        type: "odata",
        transport: {
            read: {
                url: "http://demos.telerik.com/kendo-ui/service/Northwind.svc/Categories",
                dataType: "jsonp"
            }
        },
        pageSize: 20,
        serverPaging: true,
        serverSorting: true
    })
};
data.products.group({ field: "CategoryID" });
data.categories.products = data.products;